import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ServicesService {

  apiUrl = 'https://quickbook.webtech-evolution.com/api';

  constructor(private http: HttpClient) { }
  httpOptions = {
    headers: new HttpHeaders({
      'Enctype': 'multipart/form-data'
    }),

  }

  getPosts(): Observable<any[]> {
    return this.http.get<any[]>(`${this.apiUrl}/getCountDoctor`,this.httpOptions);
  }

  getSpecialist(): Observable<any[]> {
    return this.http.get<any[]>(`${this.apiUrl}/getCountCategory`,this.httpOptions);
  }

  getservices(): Observable<any[]> {
    return this.http.get<any[]>(`${this.apiUrl}/getservices`,this.httpOptions);
  }
  getDoclist(): Observable<any[]> {
    return this.http.get<any[]>(`${this.apiUrl}/getdoctors`,this.httpOptions);
  }
  getTestimonial(){
    return this.http.get<any[]>(`${this.apiUrl}/getTestimonial`,this.httpOptions);

  }
  getblogs(){
    return this.http.get<any[]>(`${this.apiUrl}/getblogs`,this.httpOptions);

  }
}
