import { Component } from '@angular/core';
import { ServicesService } from '../services.service';


@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
  
})
export class HomePageComponent {
  posts: any = [];
  totaldoccount: any;
  specialist: any = [];
  specialistcount: any;
  services: any = [];
  doclist:any =[];
  Testimonial: any=[];
  Blogs: any=[];
  constructor(private ServicesService: ServicesService) { }

  slides = [
    { img: "../../assets/images/doctor.png", name: "DR. Addition Smith" },
    { img: "../../assets/images/doctor.png", name: "DR. Addition Smith" },
    { img: "../../assets/images/doctor.png", name: "DR. Addition Smith" },
    { img: "../../assets/images/doctor.png", name: "DR. Addition Smith" },
    { img: "../../assets/images/doctor.png", name: "DR. Addition Smith" }
  ];
  slideConfig = { "slidesToShow": 3, "slidesToScroll": 3 };

  drTestimonials = { "slideToShow": 1, "slideToScroll": 1 };

  ngOnInit() {

    this.getDoctorCount();
    this.getSpecialistCount();
    this.getService();
    this.getDoclist();
    this.getTestimonial();
    this.getblogs();
  }

  getDoctorCount() {
    this.ServicesService.getPosts().subscribe((data: any[]) => {
      this.posts = data;
      this.totaldoccount = this.posts.result.getCountDoctor;
      console.log("doctor", this.totaldoccount);
    });
  }

  getSpecialistCount() {
    this.ServicesService.getSpecialist().subscribe((data: any[]) => {
      this.specialist = data;
      this.specialist = this.specialist.result.getCountCategory;
      console.log("specialist", this.specialist);

    });
  }

  getService() {
    this.ServicesService.getservices().subscribe((data: any[]) => {
      this.services = data;
      this.services =  this.services.result.all_services;
      console.log("Services", this.services);

    });
  }
  getDoclist(){
    this.ServicesService.getDoclist().subscribe((data: any[]) => {
      this.doclist = data;
      this.doclist = this.doclist.result.doctor;


      console.log("DocList", this.doclist);

    });
  
  }
  getTestimonial(){
    this.ServicesService.getTestimonial().subscribe((data: any[]) => {
      this.Testimonial = data;
      this.Testimonial = this.Testimonial.result.testimonial;

      console.log("Testimonial", this.Testimonial);

    });
  }
  getblogs(){
    this.ServicesService.getblogs().subscribe((data: any[]) => {
      this.Blogs = data;
      this.Blogs = this.Blogs.result.blog;

      console.log("getblogs", this.Blogs);

    });
  }
}
