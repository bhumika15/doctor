import { Component } from '@angular/core';
import { ServicesService } from '../services.service';

@Component({
  selector: 'app-hospital-overview',
  templateUrl: './hospital-overview.component.html',
  styleUrls: ['./hospital-overview.component.css']
})
export class HospitalOverviewComponent {
  Testimonial: any = [];
  constructor(private ServicesService: ServicesService) { }

  drTestimonials = { "slideToShow": 1, "slideToScroll": 1 };

  ngOnInit() {
    this.getTestimonial();
  }

  getTestimonial() {
    this.ServicesService.getTestimonial().subscribe((data: any[]) => {
      this.Testimonial = data;
      this.Testimonial = this.Testimonial.result.testimonial;
      console.log("Testimonial", this.Testimonial);

    });
  }



}
