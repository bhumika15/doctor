import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HospitalOverviewComponent } from './hospital-overview.component';

describe('HospitalOverviewComponent', () => {
  let component: HospitalOverviewComponent;
  let fixture: ComponentFixture<HospitalOverviewComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [HospitalOverviewComponent]
    });
    fixture = TestBed.createComponent(HospitalOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
