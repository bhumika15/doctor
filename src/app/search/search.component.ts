import { Component } from '@angular/core';
import { ServicesService } from '../services.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent {
  posts: any = [];
  totaldoccount: any;
  specialist: any = [];
  services: any = [];
  Testimonial: any = [];
  constructor(private ServicesService: ServicesService) { }

  slideConfig = { "slidesToShow": 4, "slidesToScroll": 3, prevArrow: '<div class="left-arrow"><img src="../assets/images/arrow-left.png"></div>', nextArrow: '<div class="right-arrow"><img src="../assets/images/arrow-right.png"></div>' };
  drTestimonials = { "slideToShow": 1, "slideToScroll": 1 };

  ngOnInit() {
    this.getDoctorCount();
    this.getSpecialistCount();
    this.getService();
    this.getTestimonial();
  }

  getDoctorCount() {
    this.ServicesService.getPosts().subscribe((data: any[]) => {
      this.posts = data;
      this.totaldoccount = this.posts.result.getCountDoctor;
      console.log("doctor", this.totaldoccount);
    });
  }


  getSpecialistCount() {
    this.ServicesService.getSpecialist().subscribe((data: any[]) => {
      this.specialist = data;
      this.specialist = this.specialist.result.getCountCategory;
      console.log("specialist", this.specialist);

    });
  }

  getService() {
    this.ServicesService.getservices().subscribe((data: any[]) => {
      this.services = data;
      this.services = this.services.result.all_services;
      console.log("Services", this.services);

    });
  }

  getTestimonial() {
      this.ServicesService.getTestimonial().subscribe((data: any[]) => {
        this.Testimonial = data;
        this.Testimonial = this.Testimonial.result.testimonial;
        console.log("Testimonial", this.Testimonial);
  
      });
  }


}
