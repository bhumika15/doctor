$(document).ready(function () {
    var counted = 0;
    $(window).scroll(function () {
        var oTop = $(".counter-div").offset().top - window.innerHeight;
        if (counted == 0 && $(window).scrollTop() > oTop) {
            $(".counter-title span").each(function () {
                var $this = $(this),
                    countTo = $this.attr("data-count");
                $({
                    countNum: $this.text(),
                }).animate(
                    {
                        countNum: countTo,
                    },

                    {
                        duration: 2000,
                        easing: "swing",
                        step: function () {
                            $this.text(Math.floor(this.countNum));
                        },
                        complete: function () {
                            $this.text(this.countNum);
                            //alert('finished');
                        },
                    }
                );
            });
            counted = 1;
        }
    });
});

$(document).ready(function () {

    $(".search-banner-slider ul").slick({
        slidesToShow: 4,
        infinite: true,
        slidesToScroll: 1,
        prevArrow: '<div class="left-arrow"><img src="../assets/images/arrow-left.png"></div>',
        nextArrow: '<div class="right-arrow"><img src="../assets/images/arrow-right.png"></div>'
    });

});